# Personal site

## Mirrors

### Source

[https://codeberg.org/Jayden295/personal-site](https://codeberg.org/Jayden295/personal-site)\
[https://gitlab.com/Jayden295/personal-site](https://gitlab.com/Jayden295/personal-site)

### Website

[https://jayden295.rocks](https://jayden295.rocks)\
[https://personal-site-ash882-f3474a15ef66ffe3a79b6bc4dbacbadb3e53736ddc.gitlab.io](https://personal-site-ash882-f3474a15ef66ffe3a79b6bc4dbacbadb3e53736ddc.gitlab.io)

## Building

1. Make sure you have the submodules also cloned in git (no need for a recursive
   pull)
2. Run `deno task build`

## License

The **content** of this website is licensed under the
[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.\
The **code** of this website is licensed under the
[AGPL-3.0-or-later](https://gnu.org/licenses/agpl.html) license.

## Credits/Third Party Licenses

This website uses
[ai.robots.txt](https://github.com/ai-robots-txt/ai.robots.txt), licensed under
the
[MIT license](https://github.com/ai-robots-txt/ai.robots.txt/blob/main/LICENSE)
to block ai crawlers.\
This website uses
[pretty-feed](https://github.com/genmon/aboutfeeds/blob/main/tools/pretty-feed-v3.xsl),
licensed under the
[CC BY-NC-SA 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/) to
style RSS feeds

This website uses logos and trademark from other individuals, logos are hosted
in [./src/assets/logos/](./src/assets/logos/), and are licensed under their own
respective licenses.
