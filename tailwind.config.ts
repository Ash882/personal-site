/** @type {import('tailwindcss').Config} */

// Look at /src/css/tailwind.css for the color scheme itself
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    colors: {
      /* background colors */
      "color-background": "rgb(var(--color-background) / <alpha-value>)",
      "color-100": "rgb(var(--color-100) / <alpha-value>)",
      "color-200": "rgb(var(--color-200) / <alpha-value>)",
      "color-selected": "rgb(var(--color-selected) / <alpha-value>)",

      /* text colors */
      "color-text": "rgb(var(--color-text) / <alpha-value>)",
      "color-link": "rgb(var(--color-link) / <alpha-value>)",
      "color-link-hover": "rgb(var(--color-link-hover) / <alpha-value>)",
      "color-link-visited": "rgb(var(--color-link-visited) / <alpha-value>)",
      "color-light-text": "rgb(var(--color-light-text) / <alpha-value>)",
    },
  },
  darkMode: ["variant", [
    "@media (prefers-color-scheme: dark) { &:not(.light *) }",
    "&:is(.dark *)",
  ]],
  plugins: [
    require("@tailwindcss/typography"),
  ],
};
