export default function ManualLocaleString(
  locale: string,
  en: string,
  fr: string,
) {
  if (locale === "fr") {
    return fr;
  } else {
    return en;
  }
}
