import { defineCollection, reference, z } from "astro:content";
import { glob } from "astro/loaders";

const blogCollection = defineCollection({
  loader: glob({ pattern: "**/[^_]*.{md,mdx}", base: "./src/content/blog" }),
  schema: ({ image }) =>
    z.object({
      title: z.string(),
      tags: z.array(reference("tags")),
      creationDate: z.date(),
      image: z.object({
        url: image(),
        alt: z.string(),
        license: z.string().optional(),
        source: z.string().optional(),
      }),
      relatedProject: reference("projects").optional(),
    }),
});

const projectsCollection = defineCollection({
  loader: glob({
    pattern: "**/[^_]*.{md,mdx}",
    base: "./src/content/projects",
  }),
  schema: ({ image }) =>
    z.object({
      name: z.string(),
      tags: z.array(reference("tags")),
      description: z.string(),
      creationDate: z.date(),
      image: z.object({
        url: image(),
        alt: z.string(),
      }),
      codeberg_url: z.string(),
      deploy_url: z.string().optional(),
      blog_post: reference("blog"),
    }),
});

const tagsCollection = defineCollection({
  loader: glob({ pattern: "**/[^_]*.json", base: "./src/content/tags" }),
  schema: z.object({
    name: z.string(),
    description: z.string(),
  }),
});

export const collections = {
  blog: blogCollection,
  projects: projectsCollection,
  tags: tagsCollection,
};
