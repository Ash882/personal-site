---
title: "Poème que j'ai du traduire à l'école"
tags: ["poem", "school", "jayden-journal", "languages/francais"]
creationDate: 2024-06-20
image:
    url: '../../../assets/2024/Four_pears.jpg'
    alt: "Quatres poires"
    license: "CC BY-SA 4.0 via Wikimedia Commons"
    source:  "https://commons.wikimedia.org/wiki/File:Four_pears.jpg"
---

## Contents

## Original

> Sunt nobis mitia poma\
> Castaneae molles et pressi copia lactis;\
> Et jam summa procul villarum culmina fumant,\
> Majoresque cadunt altis de montibus umbrae.

Écrit par Publius Vergilius Maro

Ceci est un extrait de la (je pense) version originale du poème et en latin j'ai
du faire ma propre traduction (en groupe), voici la traduction

> Nous aurons un savoureux goûter\
> Des bananes mûres et beaucoups de fruits\
> Un paysage coloré et des moineaux qui chantent\
> Et ses montagnes majestueuses qui nous offrent de grandes ombres

Vous pouvez probablement voire des ereurs de français, c'est parce que il y a
trop de règles grammaticales/je sais pas quoi en français.

### Storytime derrière ce poème

C'est aussi une entrée dans mon journal du coup cela ferai plus de sense si et
bien je parlerais de ce qui c'était passer.

Alors, en Latin (_avec la prof avec un haut niveau d'autorité_ /sarcasme), il
fallait traduire ce poème en français en l'interprétant nous mêmes. J'étais dans
un groupe avec quelques personnes. C'est moi qui a écris la première version de
ce poème (VOSTFRFR++ c'est le poème écris en haut).

Mais y'avais les autres membres du groupes qui ce sont dit qu'il fallait le
modifié. Le faire "plus près" de la version originale en Latin, du coup ils ont
beaucoups modifié. Et j'était triste (non pas vraiment) donc c'est pour ça qu'il
y a la première version ici.

Et voila, c'est tout, une p'tit histoire derrière ce poème, rien d'incroyable...

À non, ceci est le premier "vrai" entrée de mon blog, le reste est plûtot en
lien avec le dévelopement du ce site web.

Merci de m'avoir lu.
