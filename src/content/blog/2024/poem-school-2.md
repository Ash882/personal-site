---
title: "D'autres poèmes que j'ai du faire au lycée"
tags: ["poem", "school", "jayden-journal", "languages/francais"]
creationDate: 2024-09-24
image:
    url: '../../../assets/2024/piano-notes.png'
    alt: "Some piano notes..."
---

## Contents

## Poems

Voici quelques poèmes que j'ai fais en français.

> La musique
>
> La musique est importante dans ma vie,\
> Je l'écoute beaucoup, je la chante,\
> Dans ma chambre, je chante des mélodies,\
> Je l'écoute, et la chante
>
> La musique est importante dans ma vie,\
> Elle raconte des histoires,\
> Des histoires qui se pairent avec ma mémoire,\
> Cela me calme, me donne de l'énergie,
>
> La musique est importante dans ma vie,\
> Ecouter chaque son et symphonie\
> Chaque accords et ondes\
> Cela me transporte dans d'autres mondes

Ce poème parle du fais que j'aime bien la musique et que elle est très
importante dans ma vie, comme le théâtre ;).

> La nuit
>
> La nuit, calme et paisible\
> Seul, froide, avec le vent rafraichissant
>
> Il y a la lune,\
> Il y a les étoiles,\
> Il y a de l'air frais,\
> Il y a le soleil couchant,\
> Je vois un paysage calme et appesant

Celui-ci parle de la nuit, je décris les moments où je rentre d'une fête et que
tout est calme et tu te sens un peu seul.

Aussi censé être un caligramme, essayer de dessiner la nuit avec cela, j'ai
essayé...

> Le théatre
>
> Au théatre, qui me permet de devenir quelque d'autre,\
> Un moyen de ce montrer, sans s'inquieter de quoi dire
>
> Tâcher de raconter l'histoire de quelqu'un d'autre,\
> Rememorrer ces moments et ces idées,\
> Et un moyen artistique de raconter une histoire

Celui-ci parle du théâtre, encore une fois, quelque chose d'important dans ma
vie, j'aime bien le théâtre, cela m'aide a m'exprimer et aussi après cela je
peux flex que je fais du théâtre donc plus de conversation, cela m'aide à être
moins timide...

Il est aussi censé être un acrostiche. Disant autre car je deviens un peu une
autre personne quand je fais du théâtre.

## Rétrospective

J'ai bien aimé faire ces poèmes, de tout de façon, si je veux faire de la
musique, je devrais essayer d'écrire des poèmes.

Tout ces poèmes ont êtes écrit le 08/09/2024, pour un DM en français.
