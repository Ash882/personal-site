---
title: "What do I do?"
tags: ["languages/english", "jayden-journal"]
creationDate: 2024-07-27
image:
    url: '../../../assets/2024/question-mark.png'
    alt: "Question mark"
---

## Contents

## Anxiety

I have no idea what to do.

I'm currently on school break, and I have practically nothing to do.

So, I watched some tiktoks and that made me want to make some kind of youtube
channel or have an online presence even more.

I especially like comedy and all of that stuff, and I already have some ideas
for sketches.

The only issue, is that I think I can't because I don't think I look that good,
and also because of my shyness. And also because I don't have a camera but that
isn't that bad, some people make sketches only using their phones (I think).

I guess, I might get into animation, you know, animate some stuff while talking
about my life.

Which is not an original idea but it's not like doing comedy is a new idea
either.

Anyways, I guess I'll probably meet my friends more, I probably feel sad also
because im not having enough social contacts.
