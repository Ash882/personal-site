---
title: "I switched this website from pnpm to Deno!"
tags: ["tech", "tech/astro", "languages/english"]
creationDate: 2024-11-20
image:
    url: '../../../assets/2024/peak-deno.png'
    alt: "Funky Deno logo made by me"
    source:  "https://codeberg.org/Jayden295/personal-site/src/branch/main/src/assets/2024/peak-deno.kra"
---

## Contents

## First of

Alright so, we are back, you might have noticed there isn't as many blog posts
as before, and to be honest, it's because I've been procrastinating and all of
that, however...

This blog/website now uses Deno instead of pnpm, I decided to use Deno because
I've seen some videos and it seems to be something with integrated features that
make it easier to develop in javascript. Oh and also because of the Typescript
native support.

## The migration process

The migration process wasn't very annoying, I just created an empty Deno project
on another directory, then moved the original project files there, fixed the
issues from the migration and done.

I oversimplified what I did, but you can see
[this commit](https://codeberg.org/Jayden295/personal-site/commit/8d9cfddd93c677501e60d2f0b638696209708bd4)
which is the one where I started the migration process, fixed some issues with
gitlab cicd and essentially, it was done.

Took me, around 2 or 3 hours to do it.

To be honest, I think I could have simply just started using Deno instead of
pnpm and changed a few files instead but, this method worked.
