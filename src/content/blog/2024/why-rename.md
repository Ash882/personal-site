---
title: "Why did I rename from Ash to Jayden"
tags: ["jayden-journal", "languages/english"]
creationDate: 2024-10-24
image:
    url: '../../../assets/2024/ash-og-pfp.png'
    alt: "Original Jayden profile picture when he was named Ash"
    license: "CC-BY-SA-4.0"
---

## Contents

## Rename

I recently renamed myself from Ash to Jayden, why?

Well because if you say Ash in french, it has the same pronounciation as the
letter H, which is problematic because they won't know if my website is
h882.codeberg.page or ash882.codeberg.page.

Also because I recently stopped really trying to make sure that the Ash and
Jayden identities were seperated, better to just put them into one same
"identity".

Small post, I know, but yeah.
