---
title: "Happy new year everyone"
tags: ["jayden-journal", "languages/english"]
creationDate: 2025-01-04
image:
  url: "../../../assets/2025/IMG_20241231_215238_288.jpg"
  alt: "Picture of my cup at new year's eve, it has a handwritten :3"
---

## Contents

## Alright, here we go.

This year has been, I think, pretty good, and, better then last year. It
definitely is better then last year, got new friends, having fun with people.
Doing theater, the usual stuff, but better, now that I think about it, there's
nothing really new this year. It makes this year a bit boring I guess.

You know what, let's be honest here, no rewriting of major parts of the blog
post, your reading raw thoughts right now, sure, if I do a typo I will obviously
fix it but there no, retries or any rewrites of major parts of the blog post.

## What do I wish?

My wishes have recently started to become more altruistic? Is that how you write
it? I wish, once again, that everyone that needs help or needs help the most,
receives their needed help. I know, writing this online will do next to nothing
to help this.

Now, I guess I'll try to give out some, mental health tips? I'm not sure if I
should.

## Rough mental health tips (hopefully it helps)

First things first, you don't have to follow them by heart, there are multiples
ways to feel fine, just, take the one that works, or don't follow any advice
here if it doesn't click with you. I may have misremembered some of them. Let's
go.

### Breath using your stomach?

Okay, I am maybe misremembering that, but, I heard that breathing though the
stomach helps you feel calmer, also, when singing/acting it is recommanded to
breath though the stomach to have more air...

Apparently it's because you feel less tension then breathing though the lungs.

### A very personal one, just, trying to realise there's no reason to feel stressed

Sometimes, I feel stressed, or anxious, instead of falling into the trap of
panicing, I try to, simply realise, there's no point in that. For example, I
remember something that I see as "cringe" that I did a few years ago, I'm going
to try to calm myself down and tell me, sure, maybe it's cringe (it isn't) but
it happened a few years ago, you don't have to worry about that anymore.

Obviously, like everything, you must balance this, don't tell yourself 500 times
that you have to calm down because it's something silly, don't overthink about
calming down. Just, find a balance.

### Listening to music can help

I don't think it makes me feel fine but, finding music that you relate to,
allows you to realise, your not alone, it helps you realise other people have
that. For example, I recently listened to
[Raised by aliens](https://youtube.com/watch?v=rpmYI4bgVFQ) by
[Ivycomb](https://youtube.com/channel/UCDOgd21T6h5XUmP9ZGoyHsw). And I really
loved some of the songs, one of them was
"[Kid Again](https://www.youtube.com/watch?t=2084&v=rpmYI4bgVFQ)" which sings
about, trying to become a kid again, innocent, happy for no reason, and also
very happy. And, I relate to that, I sometimes feel like my childhood failed,
and I think it maybe did, and I feel bittersweet, now I'm kind of trying to be
more happy, to be able to accept that.

This makes me think of a quote that I think goes like, you never recover from
grief you just learn to move on with it. Now I know that, sure, my childhood
kind of failed due to silly reasons it's not as bad as losing someone and, well,
what if I told you that this is what happened, I moved out, forgot friends,
forgot very good friends, forgot best friends, and now here I am, feeling alone
and empty.

So here are some artists that make "sad" music or at least, not just silly love
music (which is still good btw).
[Ivycomb](https://youtube.com/channel/UCDOgd21T6h5XUmP9ZGoyHsw),
[YonKaGor](https://youtube.com/@YonKaGor), now that I think about it, that's a
lot of furry artists...

## Final disclaimer which should maybe be at the start

I'm fine, better then before, don't worry. Other thing is I feel like I wrote a
bunch of dumb stuff, things that could harm people if they follow it. And I
don't want to do that, so, let's just say, these are just some words for myself,
don't follow any advice I wrote here, I may be and are wrong. Just, seek
professional help, it really does help.

Oh and don't worry guys, I'm not going to be venting here though every blog
post, only did this cause it was new year and yeah!
