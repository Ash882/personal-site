---
title: "(i gave up) (PART 2) I really like this new Ivycomb/Stephanafro album so I kind of wrote an essay about it"
tags: ["music", "languages/english"]
creationDate: 2025-01-01
image:
  url: "../../../assets/2024/raised-by-aliens-album-recreation.png"
  alt: "The Raised by Aliens album cover redrawn because I don't have an idea for an illustration"
  source: "https://codeberg.org/Jayden295/personal-site/src/branch/main/src/assets/2024/raised-by-aliens-album-recreation.kra"
---

## Contents

### Raised by Aliens

When I first heard the name of the album, I thought it was going to be about
some kind of fantasy of someone raised by aliens (after all, we got anti villain
so....).

I was obviously wrong.

So we start with verse 1, where we see that the mother is saying that they can't
be this stressed, that it's okay to not be the best, just have some fun.

And then you get an "I guess", which makes me think that they're probably in
they are a teenager now. Just saying an "I guess" to say, your right but, I
don't know, your advice doesn't seem to work.

Then you get "Look at my phone, I see a million people I don't know", the rise
of social media, influencers, millions of people talking about themselves,
playing games, and having fun.

"I'm just a kid who can't get rid of feelin' way too slow", your a kid, your
like: "oh if only I could be like them, if only I could have as many friends as
Y, if only I could be as smart as E, etc....".

Now we get into the chorus!

> I look around at these strange stars that I see\
> I can't help but feel like\
> they started to shape me I'm raised by aliens

Which talks about how, the strange stars being a hint to "When nights get dark,
just look to the stars". However I think by strange stars they mean the
influencers. After all, they are the "stars of the web!".

And the character feels like they have been raised by them, raised by those
influencers, raised by the internet.

Which makes a link to the album description which says that it talks about
"growing up in the information age".

### Turns out I stopped...

I originally wanted to finish this, this was on my backlog for a while, but in
the end, I think im just going to enjoy the awesome ivycomb music and analyse it
myself alone.

Still was something very fun to write, I remember the exitement I had, can't
believe I wrote that much just for a simple album.
