---
title: "My Personal Site!"
tags: ["languages/english", "tech/astro", "explaining-my-code"]
creationDate: 2024-07-10
image:
    url: '../../../assets/jayden-300px.png'
    alt: "Jayden's profile picture"
relatedProject: my-projects/personal-site
---

## Contents

## Welcome

This is my personal website, built on Astro.

I've chosen Astro because apparently it's a good web framework.

I like it.

I think it's the first project that I mostly did by myself (didn't use any
tutorials, but still did some web searches, but didn't use any video
tutorials).\
Which makes me proud of it, since I learned a framework by only using it's
official documentation and some web searches.

Information about the design of this website is
[in this blog post](https://jayden295.codeberg.page/blog/post/archive/site-design/)
which is probably outdated or inaccurate, I'll maybe find a better way to handle
the design later.

## Languages on this website

Blog posts are not translated, but may be in french or english.\
The rest of the website should be translated.

More information about the website (credits, etc...) in the
[README file](https://codeberg.org/Jayden295/personal-site/src/branch/main/README.md).
