---
title: "My NixOS dotfiles!"
tags: ["languages/english", "tech/nix", "explaining-my-code", "nixos"]
creationDate: 2024-07-05
image:
    url: '/src/assets/logos/nix-snowflake-colours.svg'
    alt: "NixOS logo"
    license: "CC-BY-4.0"
    source: "https://github.com/NixOS/nixos-artwork/blob/master/logo/README.md"
relatedProject: my-projects/nixos-dotfiles
---

## Contents

## What is that?

NixOS dotfiles are the dotfiles im using for my NixOS system.

Originally, they were written without using NixOS flakes, and I put all of my
configuration in a giant configuration.nix file, it was, fine.

I think it used 700 or 800 lines, but was kinda annoying to edit.

So then I heard about NixOS flakes from
[Vimjoyer](https://youtube.com/@vimjoyer) and decided to try to use it.

My initial implementation using Flakes just divided the configuration into a
bunch of files. Which was better? But there was another problem, the fact that I
couldn't find where my configuration is.

Sometimes I would change a configuration, and then would get a error like:
"system.neovim already defined in", it wasn't telling me which file had the
incorrect config so I had to search though every single file to check where it
was defined.

Then [Vimjoyer](https://youtube.com/@vimjoyer) released
[a video talking about great NixOS practices](https://youtube.com/watch?v=vYc6IzKvAJQ),
which really helped me understand how should I structure my configuration.

Following that, I decided to rewrite my whole NixOS dotfiles, which was put in
the back burner for like, a few months...

Then I worked on [my website](https://jayden295.codeberg.page/) and needed some
[content](https://youtube.com/watch?v=5XWEVoI40sE&t=428s), I needed to write the
very blog post your reading right now.

So of course, I rewrote my whole configuration, and im now proud of it. I think
that there may be still some issues about this configuration, but we'll see...
