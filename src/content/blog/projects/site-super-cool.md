---
title: "Mon site super mega cool!"
tags: ["languages/francais", "tech/vanilla-web", "tech"]
creationDate: 2024-09-22
image:
    url: '/src/assets/2024/roblox-avatar-colere.jpg'
    alt: "Avatar roblox en colère"
relatedProject: archived-projects/site-super-cool
---

## Contents

## C'est quoi?

Ceci est un petit site que j'ai fais avec du HTML et CSS en classe de SNT (ou
science du numérique, je sais pas, mon emploin du temps dis science du
numérique).

Il a été coder avec simplement du HTML et CSS avec l'éditeur de texte simple
qu'utilise GNOME/Linux (oui, mon lycée utilise Linux sur quelques ordinateurs!).

J'ai fais ça en moins d'une heure, j'ai essayer de le designer comme les anciens
sites web, et aussi de l'ecrire comme ces anciens sites web (Web 1.0), je me
suis bien amusé.

Le professeur m'avait demandé si j'avais un peu d'expèrience et bien sûr je lui
ai dit oui, de toute de façon, regarder
[mon Codeberg](https://codeberg.org/Jayden295), on peut au moins voir que
j'essaye quelque chose!

Je vais peut-être le mettre à jour ou faire un site qui lui ressemble au futur,
si je dois faire en devoir un autre site/le mettre à jour.
