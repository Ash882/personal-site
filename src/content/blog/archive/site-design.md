---
title: "Explaining my code: Design decisions for my personal site"
tags: ["languages/english", "explaining-my-code", "tech"]
creationDate: 2024-07-10
image:
    url: '../../../assets/jayden-300px.png'
    alt: "Jayden's profile picture"
---

## Contents

## Outdated, just there for historical purposes.

v0.0.1

## Responsible Web Design

Only the md- prefix will be used, the rest will be for other devices.\
I decided to do this to keep this website design consistent, so that I don't use
the md- prefix somewhere, then the sm- prefix somewhere else.

## Design system

I'll use large border radius (rounded-full) to convey friendliness and also
because I like that.

### Colors

Shades of 100, 200, 600, 700, 900, and full are used

### Sizing

0.5 ( 2px)\
1 ( 4px) 2 ( 8px) 5 ( 20px)\
6 ( 24px)\
7 ( 28px)\
12 ( 48px)\
44 (176px)\
48 (192px)\
6xl (72rem)

### Durations

Using 200ms for anything using durations (transition-200)

### Opacity

Using 90% for backgrounds (bg-slate-100/90)

### Blur

Using blur-xl (24px)

### Shadow

Using shadow-xl, shadow-sm

### Text

Using text-sm, text-base, text-lg, text-xl, text-2xl

### Rounded

rounded-lg rounded-2xl
