---
name: "My NixOS dotfiles"
tags: ["languages/english", "tech", "nixos", "tech/nix"]
description: NixOS dotfiles that I personally use.
creationDate: 2024-04-25
image:
    url: '/src/assets/logos/nix-snowflake-colours.svg'
    alt: "NixOS logo"
codeberg_url: https://codeberg.org/Jayden295/NixOS_dotfiles
blog_post: "projects/nixos-dotfiles"
---
