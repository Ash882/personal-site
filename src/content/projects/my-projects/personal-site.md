---
name: "My Personal Site"
tags: ["languages/english", "languages/francais", "tech", "tech/astro"]
description: The very website your watching right now.
creationDate: 2024-06-05
image:
    url: '../../../assets/jayden-300px.png'
    alt: "Jayden's profile picture"
codeberg_url: https://codeberg.org/Jayden295/personal-site
deploy_url: https://jayden295.codeberg.page
blog_post: "projects/personal-site"
---
