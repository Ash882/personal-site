---
name: "Trivia Game 10"
tags: ["languages/english", "tech/vanilla-web", "tech"]
description: A simple trivia game
creationDate: 2024-05-18
image:
    url: '../../../assets/projects/archived/trivia-game-10-logo.webp'
    alt: "Trivia Game 10 logo, a bunch of blocks standing on each other"
codeberg_url: https://codeberg.org/Jayden295/Trivia_Game_10
deploy_url: https://jayden295.codeberg.page/Trivia_Game_10/@latest/
blog_post: "projects/trivia-game-10"
---
