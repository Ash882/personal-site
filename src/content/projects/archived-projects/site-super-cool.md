---
name: "Mon site super mega cool!"
tags: ["languages/francais", "tech/vanilla-web", "tech"]
description: un site super mega cool
creationDate: 2024-09-17
image:
    url: '/src/assets/2024/roblox-avatar-colere.jpg'
    alt: "Avatar roblox en colère"
codeberg_url: https://codeberg.org/Jayden295/mon_site_super_mega_cool
deploy_url: https://jayden295.codeberg.page/mon_site_super_mega_cool//
blog_post: "projects/site-super-cool"
---
