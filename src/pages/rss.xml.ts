import rss from "@astrojs/rss";
import { type CollectionEntry, getCollection } from "astro:content";
import type { APIContext } from "astro";

export async function GET(context: APIContext) {
  const blog = await getCollection("blog");

  blog.sort(
    (a: CollectionEntry<"blog">, b: CollectionEntry<"blog">) =>
      new Date(b.data.creationDate).getTime() -
      new Date(a.data.creationDate).getTime(),
  );

  return rss({
    stylesheet: "/rss/pretty-feed-v3.xsl",
    title: "Jayden's blog",
    description: "Let me tell you about my life!",
    site: context.site as URL,
    items: await Promise.all(
      blog.map((post: CollectionEntry<"blog">) => ({
        title: post.data.title,
        pubDate: post.data.creationDate,
        link: `blog/post/${post.id}`,
      })),
    ),
  });
}
