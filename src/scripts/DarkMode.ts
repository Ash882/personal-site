// Handles dark/light mode
function removeFromArray(array: Array<string>, string: string) {
  const index = array.indexOf(string);
  if (index > -1) array.splice(index, 1);
}

const valid_themes = ["auto", "dark", "light"];

const DarkModeSwitchers = document.getElementsByClassName("dark-mode-switcher");

// Loads theme from localstorage and
// sets the theme in <html> by adding it as a class
// and sets the value in dark mode switchers as well
function load_theme() {
  const saved_theme = localStorage.getItem("theme");

  if (saved_theme == null) return;

  if (valid_themes.indexOf(saved_theme) <= -1) {
    console.warn(
      "theme stored in local storage is invalid, not loading theme localStorage theme:",
      saved_theme,
    );
    return;
  }

  if (saved_theme != "auto") {
    document.documentElement.classList.add(saved_theme);
  }

  for (const element of DarkModeSwitchers) {
    if (element instanceof HTMLSelectElement) {
      element.value = saved_theme;
    }
  }
}

load_theme();
document.addEventListener("astro:after-swap", load_theme);

// Loop though every dark mode switcher to listen for changes
for (const current_dark_mode_switcher of DarkModeSwitchers) {
  current_dark_mode_switcher.classList.remove("hidden");

  // When it changes update the other dark mode switchers
  // Set the current theme in <html>
  // Set the saved value in local storage
  current_dark_mode_switcher.addEventListener(
    "change",
    function (this: HTMLSelectElement) {
      const potential_class = [...valid_themes];

      // Make sure potential class is valid before doing anything
      if (potential_class.indexOf(this.value) <= -1) {
        console.warn(
          "new value of dark mode switcher is not expected, not changing theme! this.value:",
          this.value,
        );
        return;
      }

      // potential_class is now used to update the theme in <html>
      // "auto" is the default so we aren't removing it
      // just read the code to get it to be honest
      removeFromArray(potential_class, "auto");

      const other_dark_mode_switcher = Array.from(
        DarkModeSwitchers,
      ).find(function (other_dark_mode_switcher) {
        return (
          other_dark_mode_switcher !== current_dark_mode_switcher
        );
      });

      // Update value is dark mode switcher exists
      // It is supposed to but if anyone modifies the website
      if (
        other_dark_mode_switcher &&
        other_dark_mode_switcher instanceof HTMLSelectElement
      ) {
        other_dark_mode_switcher.value = this.value;
      }

      removeFromArray(potential_class, this.value);

      // Remove class names that need to be removed
      for (const class_name of potential_class) {
        document.documentElement.classList.remove(class_name);
      }

      // Auto is the default, so don't add it
      if (this.value != "auto") {
        document.documentElement.classList.add(this.value);
      }

      localStorage.setItem("theme", this.value);
    },
  );
}
